#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iomanip>
using namespace std;
void FIFO(int *frame, int *ref, int sizeframe, int sizeref);
void optimal(int *frame, int *ref, int sizeframe, int sizeref);
void LRU(int *frame, int *ref, int sizeframe, int sizeref);
int main() {
	int sizeframe = 3;
	int sizeref = 20;
	int seed = 0;
	do {
		cout << "Algorithms" << endl << "1) FIFO [1]" << endl << "2) Optimal [2]" << endl << "3) LRU [3]" << endl << "Choose : ";
		int choose;
		cin >> choose;
		cout << "Number of Page frame : "; cin >> sizeframe;
		cout << "Number of Refference string : "; cin >> sizeref;
		cout << "seed: "; cin >> seed;
		srand(seed);

		int *frame = new int[sizeframe];
		int *ref = new int[sizeref];
		for (int i = 0; i < sizeframe; i++)
		{
			frame[i] = -1;
		}
		cout << "Refference string : ";
		for (int i = 0; i < sizeref; i++)
		{
			ref[i] = rand() % 10 + 1;
			cout << ref[i] << " ";
		}

		if (choose == 1) {
			FIFO(frame, ref, sizeframe, sizeref);
		}
		else if (choose == 2) {
			optimal(frame, ref, sizeframe, sizeref);
		}
		else if (choose == 3) {
			LRU(frame, ref, sizeframe, sizeref);
		}
		char con = 'n';
		cout << endl<<endl<<"continue ?"; cin >> con;
		if (con == 'n') break;
		system("CLS");
	}
	while (true);
	system("pause");
}

void FIFO(int *frame, int *ref, int sizeframe, int sizeref) {
	cout << endl;
	cout << "------------------"<<endl;
	cout << "||FIFO Algorithm||"<<endl;
	cout << "------------------";
	int page_fault = 0;
	int swapper = 0;
	for (int i = 0; i < sizeref; i++) {
		cout << endl << setw(5) << ref[i] << " : ";
		for (int i = 0; i < sizeframe; i++) {
			if (frame[i] == -1) {
				cout << setw(8) << "[  X ]";

			}
			else {
				cout << setw(4) << "[ " << setw(2) << frame[i] << " ]" << setw(2);
			}
		}
		int pushed = 0;
		for (int i_frame = 0; i_frame < sizeframe; i_frame++) {
			if (frame[i_frame] == ref[i]) {//�� ������� frame
				pushed = 1;
				break;
			}
		}
		if (pushed == 0) {
			for (int i_frame = 0; i_frame < sizeframe; i_frame++) {
				if (frame[i_frame] == -1) {
					page_fault++;
					cout << "-->Page fault";

					frame[i_frame] = ref[i];
					pushed = 1;
					break;
				}
			}
		}
		if (pushed == 0) {
			frame[swapper] = ref[i];
			page_fault++;
			cout << "-->Page fault";
			swapper++;
			pushed = 1;
			if (swapper == sizeframe) {
				swapper = 0;
			}
		}
	}
	cout << endl << setw(8) << " : ";
	for (int i = 0; i < sizeframe; i++) {
		if (frame[i] == -1) {
			cout << setw(8) << "[  X ]";

		}
		else {
			cout << setw(4) << "[ " << setw(2) << frame[i] << " ]" << setw(2);
		}
	}
	cout <<endl<<"Page fault : "<< page_fault;
}

void optimal(int *frame, int *ref, int sizeframe, int sizeref) {
	cout << endl;
	cout << "---------------------" << endl;
	cout << "||Optimal Algorithm||" << endl;
	cout << "---------------------" ;
	int page_fault = 0;
	for (int i = 0; i < sizeref; i++)
	{
		cout << endl << setw(5) << ref[i] << " : ";
		for (int i = 0; i < sizeframe; i++) {
			if (frame[i] == -1) {
				cout << setw(8) << "[  X ]";

			}
			else {
				cout << setw(4) << "[ " << setw(2) << frame[i] << " ]" << setw(2);
			}
		}
		int pushed = 0;
		for (int i_frame = 0; i_frame < sizeframe; i_frame++) {
			if (frame[i_frame] == ref[i]) {//�� ������� frame
				frame[i_frame] = ref[i];
				pushed = 1;
				break;
			}
		}
		if (pushed == 0) {
			for (int i_frame = 0; i_frame < sizeframe; i_frame++) {
				if (frame[i_frame] == -1) {
					page_fault++;
					cout << "-->Page fault";
					frame[i_frame] = ref[i];
					pushed = 1;
					break;
				}
			}
		}
		if (pushed == 0) {
			int swapper = 0;
			int maxindex = -1;
			bool found;

			for (int i_swapper = 0; i_swapper < sizeframe; i_swapper++)
			{
				found = false;
				for (int i_ref = i; i_ref < sizeref; i_ref++)
				{
					if (frame[i_swapper] == ref[i_ref]) {
						if (maxindex < i_ref) {
							swapper = i_swapper;
							maxindex = i_ref;
						}
						found = true;
						break;
					}
				}
				if (found == false) {
					frame[i_swapper] = ref[i];
					pushed = 1;
					break;
				}
			}
			if (pushed == 0) {
				frame[swapper] = ref[i];
			}
			page_fault++;
			cout << "-->Page fault";
		}
	
	}
	cout << endl << setw(8) << " : ";
	for (int i = 0; i < sizeframe; i++) {
		if (frame[i] == -1) {
			cout << setw(8) << "[  X ]";

		}
		else {
			cout << setw(4) << "[ " << setw(2) << frame[i] << " ]" << setw(2);
		}
	}
	cout << endl << "Page fault : " << page_fault;

}

void LRU(int *frame, int *ref, int sizeframe, int sizeref) {
	cout << endl;
	cout << "-----------------" << endl;
	cout << "||LRU Algorithm||" << endl;
	cout << "-----------------";
	int page_fault = 0;
	for (int i = 0; i < sizeref; i++)
	{
		cout << endl<<setw(5)<<ref[i]<<" : ";
		for (int i = 0; i < sizeframe; i++) {
			if (frame[i] == -1) {
				cout << setw(8) << "[  X ]";

			}
			else {
				cout << setw(4) << "[ " << setw(2) << frame[i] << " ]" << setw(2);
			}
		}
		int pushed = 0;
		for (int i_frame = 0; i_frame < sizeframe; i_frame++) {
			if (frame[i_frame] == ref[i]) {//�� ������� frame
				frame[i_frame] = ref[i];
				pushed = 1;
				break;
			}
		}
		if (pushed == 0) {
			for (int i_frame = 0; i_frame < sizeframe; i_frame++) {
				if (frame[i_frame] == -1) {
					page_fault++;
					cout << "-->Page fault";

					frame[i_frame] = ref[i];
					pushed = 1;
					break;
				}
			}
		}
		if (pushed == 0) {
			int swapper = 0;
			int minindex = sizeref + 1;

			for (int i_swapper = 0; i_swapper < sizeframe; i_swapper++)
			{
				for (int i_ref = i; i_ref >= 0; i_ref--)
				{
					if (frame[i_swapper] == ref[i_ref]) {
						if (minindex > i_ref) {
							swapper = i_swapper;
							minindex = i_ref;
						}
						break;
					}
				}
			}
			if (pushed == 0) {
				frame[swapper] = ref[i];
			}

			page_fault++;
			cout << "-->Page fault";

		}
	
	}
	cout << endl << setw(8) << " : ";
	for (int i = 0; i < sizeframe; i++) {
		if (frame[i] == -1) {
			cout << setw(8) << "[  X ]";

		}
		else {
			cout << setw(4) << "[ " << setw(2) << frame[i] << " ]" << setw(2);
		}
	}
	cout << endl << "Page fault : " << page_fault;
}
